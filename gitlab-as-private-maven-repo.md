<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>gitlab-as-private-maven-repo</title>
  <link rel="stylesheet" href="https://stackedit.io/style.css" />
</head>

<body class="stackedit">
  <div class="stackedit__html"><h1 id="artefactory-для-бомжей-умных-и-экономных-специалистов">Artefactory для <s>бомжей</s> умных и экономных специалистов</h1>
<blockquote>
<p>GitLab как приватный Maven репозиторий</p>
</blockquote>
<h2 id="вступление">Вступление</h2>
<p><strong>Задача:</strong> создать приватный Maven репозиторий для хранения артефактов внутренних проектов внутри компании. Ограничить третьим лицам доступ к внутренним продуктам и решениям компании. Стоимость решения (не считая железа для размещения решения) не должна превышать <strong>0 руб. 0. коп.</strong><br>
<strong>Решение:</strong> основой хранилища является git-репозиторий (GitLab) т.к.:</p>
<ul>
<li>легко разместить на собственном железе</li>
<li>поддержка версионирования (средствами VCS Git)</li>
<li>управление правами доступа как для сотрудников, так и третьих лиц. (GitLab)</li>
</ul>
<p>Для интеграции <code>maven</code> и <code>git</code> используется плагин <a href="https://synergian.github.io/wagon-git/index.html">wagon-git</a></p>
<h2 id="инструкция-по-использованию">Инструкция по использованию</h2>
<h3 id="требования-к-рабочему-окружению">Требования к рабочему окружению</h3>
<ol>
<li>
<p>В системе должен быть установлен <a href="https://git-scm.com/">Git</a> и добавлен в <code>PATH</code></p>
</li>
<li>
<p>В системе должен быть установлен <a href="https://maven.apache.org/">Maven 3.3+</a></p>
</li>
<li>
<p>Должен быть доступ к <a href="https://gitlab.com/reagentum/maven-repository">проекту на GitLab</a> с правами:</p>
<ul>
<li><code>read</code>, для использования артефактов</li>
<li><code>write</code>, для выполнения поставок (<code>deploy</code>) артефактов</li>
</ul>
</li>
</ol>
<h3 id="настройка-окружения">Настройка окружения</h3>
<h4 id="использование-артефактов">1. Использование артефактов</h4>
<p>Для использования артефактов необходимо добавить в <code>pom.xml</code> проекта (или глобально в <code>settings.xml</code>) следующие репозитории:</p>
<ul>
<li>
<p><strong>Releases</strong> (основной репозиторий с production-ready кодом):</p>
<pre><code>&lt;repository&gt;
    &lt;id&gt;reagentum-maven-repo&lt;/id&gt;
    &lt;name&gt;Reagentum Maven repo&lt;/name&gt;
    &lt;releases&gt;
        &lt;enabled&gt;true&lt;/enabled&gt;
    &lt;/releases&gt;
    &lt;snapshots&gt;
        &lt;enabled&gt;false&lt;/enabled&gt;
    &lt;/snapshots&gt;
    &lt;url&gt;https://gitlab.com/reagentum/maven-repository/raw/releases&lt;/url&gt;
&lt;/repository&gt;
</code></pre>
</li>
<li>
<p><strong>Snapshots</strong> (нестабильные, dev-версии артефактов)</p>
<pre><code>&lt;repository&gt;
    &lt;id&gt;reagentum-maven-snapshot-repo&lt;/id&gt;
    &lt;name&gt;Reagentum Maven snapshot repo&lt;/name&gt;
  	&lt;releases&gt;
        &lt;enabled&gt;false&lt;/enabled&gt;
    &lt;/releases&gt;
    &lt;snapshots&gt;
        &lt;enabled&gt;true&lt;/enabled&gt;
   &lt;/snapshots&gt;
   &lt;url&gt;https://gitlab.com/reagentum/maven-repository/raw/snapshots&lt;/url&gt;
&lt;/repository&gt;
</code></pre>
</li>
</ul>
<h4 id="поставка-deploy-артефактов">2. Поставка (deploy) артефактов</h4>
<p>В <code>pom.xml</code> проекта добавить следующие секции:</p>
<pre><code>&lt;pluginRepositories&gt;  
    &lt;pluginRepository&gt;  
        &lt;id&gt;synergian-repo&lt;/id&gt;  
        &lt;url&gt;https://raw.github.com/synergian/wagon-git/releases&lt;/url&gt;  
    &lt;/pluginRepository&gt;
&lt;/pluginRepositories&gt;
&lt;distributionManagement&gt;  
    &lt;repository&gt;  
        &lt;id&gt;reagentum-maven-repo&lt;/id&gt;  
        &lt;name&gt;Reagentum maven repo&lt;/name&gt;  
        &lt;url&gt;git:releases://git@gitlab.com:reagentum/maven-repository.git&lt;/url&gt;  
    &lt;/repository&gt;  
    &lt;snapshotRepository&gt;  
        &lt;id&gt;reagentum-maven-snapshot-repo&lt;/id&gt;  
        &lt;name&gt;Reagentum maven snapshot repo&lt;/name&gt;  
        &lt;url&gt;git:snapshots://git@gitlab.com:reagentum/maven-repository.git&lt;/url&gt;  
    &lt;/snapshotRepository&gt;
&lt;/distributionManagement&gt;
</code></pre>
<p>В секцию <code>build</code> добавить следующее:</p>
<pre><code>&lt;build&gt;
    ...
    &lt;plugins&gt; ... &lt;/plugins&gt;
	...
	&lt;extensions&gt;  
	    &lt;extension&gt;  
	        &lt;groupId&gt;ar.com.synergian&lt;/groupId&gt;  
	        &lt;artifactId&gt;wagon-git&lt;/artifactId&gt;  
	        &lt;version&gt;${wagon.git.version}&lt;/version&gt;  
	    &lt;/extension&gt;
    &lt;/extensions&gt;
&lt;/build&gt;
</code></pre>
<p>Для поставки артефакта необходимо выполнить команду <code>mvn clean deploy</code> В зависимости от версии (<code>-SNAPSHOT</code>) расширение автоматически выполнит установку зависимости в <code>realeases</code> или <code>snapshot</code> репозиторий.</p>
<h4 id="установка-сторонних-артефактов">3. Установка сторонних артефактов</h4>
<p>Для установки сторонних артефактов необходимо выполнить команду:</p>
<pre><code>mvn deploy:deploy-file \
    -DgroupId=somegroup \
    -DartifactId=thirdparty \
    -Dversion=1.1.1 \
    -Dfile=a-third-party-jar.jar \
    -DgeneratePom=true \
    -DrepositoryId=reagentum-maven-repo \
    -Durl=git:releases://git@gitlab.com:reagentum/maven-repository.git
</code></pre>
</div>
</body>

</html>

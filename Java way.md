<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Java way</title>
  <link rel="stylesheet" href="https://stackedit.io/style.css" />
</head>

<body class="stackedit">
  <div class="stackedit__html"><h2 id="если-нету-опыта-в-java-совсем">Если нету опыта в Java совсем</h2>
<ul>
<li><a href="https://www.ozon.ru/context/detail/id/142431463/">Философия Java</a></li>
<li><a href="https://www.ozon.ru/context/detail/id/7821666/">Изучаем Java</a></li>
</ul>
<p>По составу у качеству материала книги примерно одинаковые, отличается только подача материала. По этому каждый выберет себе вариант по душе.</p>
<ul>
<li><a href="https://www.ozon.ru/context/detail/id/147725679/">Git для профессионального программиста</a> - без этого никуда</li>
<li><a href="https://www.ozon.ru/context/detail/id/28336354/">Чистый код: создание, анализ и рефакторинг</a> - не читал - не мужик</li>
</ul>
<h2 id="прокачиваем-средний-уровень">Прокачиваем средний уровень</h2>
<ul>
<li><a href="https://www.ozon.ru/context/detail/id/148627191/">Java. Эффективное программирование</a> - лучшие практики и советы от разработчика языка. <strong>Обязательна к прочтению</strong></li>
<li><a href="https://www.ozon.ru/context/detail/id/23529814/">Структуры данных и алгоритмы в Java</a> - не только покажет как использовать Java, но и расскажет об основных алгоритмах и структурах данных. Как по мне также <strong>обязательна к прочтению</strong></li>
<li><a href="https://www.ozon.ru/context/detail/id/145346364/">Современный Java. Рецепты программирования</a> - расскажет о последних фишках языка. Актуальный материал без воды. Крайне рекомендую ознакомиться</li>
</ul>
<h2 id="все-что-нужно-чтобы-гордо-носить-звание-крепкий-middle">Все, что нужно, чтобы гордо носить звание “Крепкий middle”</h2>
<ul>
<li><a href="https://www.ozon.ru/context/detail/id/146748538/">Java в облаке. Spring Boot, Spring Cloud, Cloud Foundry</a> - Актуальный материал по разработке на Java “под облака”. Наиболее полярная и хайповая тема на данный момент (и на будущее тоже)</li>
<li><a href="https://www.ozon.ru/context/detail/id/139989481/">Реактивное программирование с использованием RxJava</a> - раскрывает популярную и актуальную тему реактивности. Ознакомиться, лишним не будет</li>
<li><a href="https://www.ozon.ru/context/detail/id/149092813/">Spring 5 для профессионалов</a> - лучшая книга по Spring. Самый популярный на данный момент фреймворк. Писать на java и не знать Spring - это можно сразу собираться и идти в javascript говнокодить.</li>
<li><a href="https://www.ozon.ru/context/detail/id/19383907/">Семь баз данных за семь недель. Введение в современные базы данных и идеологию NoSQL</a> - отличный обзор современных NoSQL решений</li>
<li><a href="https://www.ozon.ru/context/detail/id/3174887/">Java Concurrency in Practice</a> - это классика, тут больше нечего добавить</li>
<li><a href="https://www.amazon.com/Java-Performance-Definitive-Guide-Getting/dp/1449358454">Java Performance: The Definitive Guide: Getting the Most Out of Your Code</a> - если освоишь, считай что ты настоящий гусар в мире java</li>
</ul>
<h2 id="для-настоящего-околофутбольщика-околоджависта">Для настоящего <s>околофутбольщика</s> околоджависта</h2>
<ul>
<li><a href="https://www.ozon.ru/context/detail/id/139411597/">Использование Docker</a> - без докера сейчас вообще никуда</li>
<li><a href="https://www.ozon.ru/context/detail/id/147815955/"> Философия DevOps. Искусство управления IT</a> - чтобы понимать как сейчас происходит процесс разработки и доставки продукта <s>конченным</s> конечным пользователям</li>
<li><a href="https://www.ozon.ru/context/detail/id/148388621/">Kubernetes в действии</a> - тоже годнота</li>
<li><a href="https://github.com/sindresorhus/awesome">Awesome lists about all kinds of interesting topics</a> - тут всегда можно найти подборку заебательских и актуальных материалов по любым направлениям. Не знаешь что выбрать для конкретной задачи? Тебе сюда</li>
</ul>
</div>
</body>

</html>

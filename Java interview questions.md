<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Java interview questions</title>
  <link rel="stylesheet" href="https://stackedit.io/style.css" />
</head>

<body class="stackedit">
  <div class="stackedit__html"><blockquote>
<p>❗ - вопросы для настоящих гусаров (<code>middle+</code>)</p>
</blockquote>
<h2 id="околоразработки">Околоразработки</h2>
<ol>
<li>Системы сборки (<code>maven</code>, <code>gradle</code>, <code>ant</code>)</li>
<li>Системы контроля версий (<code>git</code>, <code>svn</code>)</li>
<li>Task tracking</li>
<li>CI\CD</li>
</ol>
<h2 id="ооп">ООП</h2>
<ol>
<li>Основные свойства ООП.
<ul>
<li>абстракция, инкапсуляция, наследование, полиморфизм - что это такое, как выражено в Java.</li>
<li>наследование и композиция, плюсы / минусы, личное предпочтение и почему</li>
<li>что такое классы объекты.</li>
<li>перегрузка и переопределение методов. В чем отличие</li>
<li>модификаторы доступа (на примере Java)</li>
<li>типы конструкторов (Конст. по умолчанию, констр. с параметрами, copy-конструктор)</li>
<li>ранняя и поздняя инициализация</li>
</ul>
</li>
<li>❗ Принципы ООП и практики ООП
<ul>
<li><strong>SOLID</strong> принципы</li>
<li>“не повторяйся” - <strong>DRY</strong></li>
<li>шаблоны (паттерны) <strong>GRASP</strong></li>
<li>“вам это не понадобится” - Вам это не понадобится</li>
<li>примеры личного применения шаблонов ООП</li>
</ul>
</li>
</ol>
<h2 id="java-core">Java core</h2>
<ol>
<li>Примитивные типы и классы обертки</li>
<li>Ключевое слово <code>static</code> (статическая инициализация)</li>
<li>Ключевое слово <code>final</code></li>
<li>Вложенные и внутренние классы, особенности и отличия</li>
<li>Анонимные классы</li>
<li>Строки в java, особенности <code>String</code>
<ul>
<li>в чем разница между <code>String</code>, <code>StringBuffer</code> и <code>StringBuilder</code></li>
<li>почему строки в java неизменяемые (<code>immutable</code>)</li>
<li>что такое пул строк</li>
<li>что делает метод <code>intern()</code> класса String</li>
<li>являются строки потокобезопасноми</li>
</ul>
</li>
<li>Отличие <code>==</code> от <code>equals</code></li>
<li>Клонирование объектов, интерфейс <code>Clonable</code></li>
<li>Сравнение объектов. Компараторы</li>
<li>Изменяемые (<code>mutable</code>) и неизменяемые (<code>immutable</code>) объекты в Java, в чем разница</li>
<li>Исключения и их обработка, ключевое слово <code>final</code>, конструкция <code>try-with-resource</code></li>
<li><code>Checked</code> и <code>unchecked</code> исключения, отличия</li>
<li>Отличия <code>Exception</code> от <code>RuntimeException</code></li>
<li>В чем разница между <code>Exception</code> и <code>Error</code></li>
<li>Сериализация - что это, привести пример.</li>
</ol>
<h2 id="java-collections">Java collections</h2>
<ol>
<li>Типы коллекций в java</li>
<li>Разница между <code>ArrayList</code>, <code>LinkedList</code> и массивами. Преимущества и недостатки “показания к применению”</li>
<li>Интерфейсы <code>Map</code> и <code>Set</code>. Их реализации, особенности и “показания к применению”</li>
<li>Устройство контейнеров на базе хеширования (<code>HashMap</code>, <code>HashSet</code>).</li>
<li>Потокобезопасные коллекции. Преимущества и недостатки.</li>
</ol>
<h2 id="java-concurrency">Java concurrency</h2>
<ol>
<li>Отличие обычных потоков от потоков-демонов</li>
<li>Способы создания потоков в Java</li>
<li><code>wait</code>, <code>notify</code>, <code>notifyAll</code></li>
<li><code>synchronized</code> секции/методы, <code>volatile</code></li>
<li>Что такое <code>ThreadLocal</code></li>
<li>Интерфейсы <code>Runnable</code>  и <code>Callable</code></li>
<li>Пулы потоков в Java</li>
<li>❗ Atomic-операции. Atomic классы из <code>java.until.concurrency</code></li>
<li>❗ Интерфейс <code>Lock</code> преимущества перед <code>synchronized</code></li>
<li>❗ <code>Executor framework</code></li>
<li>❗ <code>Fork/join framework</code></li>
<li>❗ <code>CompletableFuture</code> (Concurrency в Java 8)</li>
</ol>
<h2 id="jee">JEE</h2>
<ol>
<li><code>JDBC</code>
<ul>
<li>что такое драйвер БД, интерфейс <code>Connection</code></li>
<li><code>Statement</code>, <code>PrepareStatemet</code> и <code>CallableStatemet</code>. Что это такое, отличия, для чего используются</li>
<li>сonnection pooling, что такое, для чего нужен, известные реализации</li>
<li>что такое <code>DataSource</code>, преимущества.</li>
<li>❗ что такое <code>ResultSet</code>, Какие бывают типы</li>
<li>транзакции и уровни изоляции</li>
</ul>
</li>
<li><code>Servlets</code>
<ul>
<li>в чем разница между web server и application server</li>
<li>в чем разница между методами <code>GET</code> и <code>POST</code></li>
<li>что такое <code>Servlet</code></li>
<li>является ли сервлет потокобезовасным. Как достичь потокобезопасности сервлета</li>
<li>❗ наиболее важные особенности <code>Servlet 3.0</code></li>
</ul>
</li>
<li><code>Web Services</code>
<ul>
<li>протокол <code>SOAP</code></li>
<li>что такое <code>XSD</code> и <code>WSDL</code></li>
<li>средства работы с <code>SOAP</code> сервисами в <code>JEE</code></li>
</ul>
</li>
<li><code>JPA</code>
<ul>
<li>что такое <code>JPA</code>, преимущества и недостатки, известные реализации</li>
<li>типичные проблемы реализаций <code>JPA</code></li>
<li>виды связей между сущностями (<code>@ManyToMany</code>, <code>@OneToMany</code> и т.д.)</li>
<li>реализация наследования таблиц</li>
<li><code>сriteria API</code></li>
<li><code>JPQL</code></li>
<li>альтернативные средства для реализации <code>DAO</code> (<code>MyBatis</code>, <code>jOOQ</code>, и др.)</li>
</ul>
</li>
</ol>
<h2 id="spring-framework">Spring framework</h2>
<ol>
<li>Что такое spring framework, для чего нужен</li>
<li>IoC и DI, что такое</li>
<li>Какие есть виды <code>DI</code> в <code>Spring</code>. Преимущество <code>constructor-injections</code> перед <code>setter-injection</code></li>
<li>Что такое <code>bean</code>, скопы бинов, скоп по умолчанию, потокобезопасность бинов</li>
<li>❗ Жизненный цикл бинов</li>
<li>Что такое <code>ApplicationContex</code></li>
<li>Способы конфигурирования spring-приложения</li>
<li>Отличия <code>@Service</code>, <code>@Repository</code>, <code>@Component</code></li>
<li>Что такое <code>DispatcherServlet</code></li>
<li>Отличия <code>@Controller</code> от <code>@RestController</code></li>
<li>Что такое <code>spring profiles</code> для чего используются</li>
<li>Аннотация <code>@Qualifier</code>, выбор реализации бина в runtime (<code>@ConditionalOn...</code>)</li>
<li>❗ AOP
<ul>
<li>что такое <code>AOP</code>, какие задачи решает</li>
<li>основные понятия: <em>Aspect</em>, <em>Join point</em>, <em>Advice</em>, <em>Pointcut</em>, <em>Introduction</em></li>
<li>типы <em>Advice</em></li>
<li>пример использования</li>
</ul>
</li>
<li>❗ Механизм проксирования в Spring. Для чего нужен, особенности. Отличия <code>JDK dynamic proxy</code> от <code>cglib</code></li>
</ol>
<h2 id="sql">SQL</h2>
<ol>
<li>Первичные и внешние ключи, что такое целостность данных</li>
<li>Что такое индексы, какие бывают, для чего нужны</li>
<li>Что такое нормализация (какие бывают формы)</li>
<li>Ключевое слово <code>HAVING</code></li>
<li>В чем разница между <code>count(*)</code> и <code>count(&lt;столбец&gt;)</code></li>
<li>Какие бывают типы выражения <code>JOIN</code>, в чем отличия</li>
<li>❗ В чем отличия между <code>DELETE</code>, <code>TRUNCATE</code> и <code>DROP</code></li>
<li>❗ Что такое <code>Execution plan</code>, для чего его можно использовать, как его получить</li>
<li>❗ В чем разница между <code>IN</code> и <code>EXISTS</code></li>
<li>Как выбрать уникальные записи из таблицы</li>
</ol>
<h2 id="microservice--clound-development">Microservice / Clound development</h2>
<blockquote>
<p>Здесь и далее идут вопросы типа “знаешь/не знаешь”, “что использовал, где и для чего”, т.е. стоит цель понять насколько хорошо кандидат ориентируется в современном стэке, без углубления в детали.</p>
</blockquote>
<ol>
<li>Паттерны проектирования микросервисной архитектуры: <code>service-discovery</code>, <code>gateway</code>, <code>fail-fast</code>, <code>fault tolerance</code>, <code>circuit breaker</code>. <code>load balancing</code></li>
<li><code>RESTFull</code></li>
<li>Реализация вышеуказанных шаблонов в <code>Spring Cloud</code>  (или аналогах)</li>
<li><code>NIO</code></li>
<li>Реактивность. Основные принципы. (<code>RxJava</code>, <code>Spring Reactor</code> или аналоги)</li>
</ol>
<h3 id="околооблаков">Околооблаков</h3>
<ol>
<li>Контерйнеризация приложений (<code>Docker</code>).</li>
<li>Оркестрация контейнеров / управление кластером. (<code>Kubernates</code>, <code>Runcher</code>, <code>Mesos + Marathon</code> и др.)</li>
<li>Мониторинг, метрики, health check.</li>
<li>Сбор логов</li>
<li>Хранение данных (<code>NoSQL</code>, <code>Key-Value</code>, <code>Cache</code>). С чем работал, для чего использовал</li>
<li>Очереди сообщений</li>
<li><code>OAuth2</code>, <code>JWT</code></li>
<li>External configurations (<code>Consul</code>, <code>Spring Cloud Config</code>, что-то другое)</li>
</ol>
<blockquote>
<p>Ссылки<br>
<a href="https://www.edureka.co/blog/interview-questions/java-interview-questions/">https://www.edureka.co/blog/interview-questions/java-interview-questions/</a><br>
<a href="https://www.journaldev.com/interview-questions">https://www.journaldev.com/interview-questions</a><br>
<a href="https://www.softwaretestinghelp.com/core-java-interview-questions/">https://www.softwaretestinghelp.com/core-java-interview-questions/</a><br>
<a href="https://career.guru99.com/top-50-j2ee-interview-questions/">https://career.guru99.com/top-50-j2ee-interview-questions/</a><br>
<a href="https://www.concretepage.com/spring/">https://www.concretepage.com/spring/</a></p>
</blockquote>
</div>
</body>

</html>
